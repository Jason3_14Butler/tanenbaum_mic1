/*
  © Copyright Jason Butler 2017 GPL
*/

`timescale 1ns / 1ps

module mem_controllertb;

reg [31:0] mem_bus_data;
reg [31:0] mem_addr;
wire [31:0] mem_bus;
reg enb_write;
reg enb_read;
reg addr_strob;

assign mem_bus = mem_bus_data;
//oscilator osc(clk,clk_n);
mem_controller uut (
.enb_write(enb_write),
.enb_read(enb_read),
.addr_strob(addr_strob),
.mem_addr(mem_addr),
.mem_bus(mem_bus)
);

initial
begin
  $dumpfile("memory_controllertb.vcd");
  $dumpvars(0,mem_controllertb);

$display("testing memory_controller");
enb_read=0;enb_write=0;addr_strob=0;
#5;mem_bus_data=32'hABFBABAB;mem_addr=32'h00000080;enb_write=1;addr_strob=1;
#5;enb_write=0;addr_strob=0;mem_bus_data=32'bz;
#5;mem_bus_data=32'hdeadbeef;mem_addr=32'h000000C0;enb_write=1;addr_strob=1;
#5;enb_write=0;addr_strob=0;mem_bus_data=32'bz;
#5;mem_addr=32'h00000080;enb_read=1;addr_strob=1;
#5;enb_read=0; addr_strob=0;
#5;mem_addr=32'h000000C0;enb_read=1;addr_strob=1;
#50;
end

initial
begin
$monitor("time = %2d,enb_write=%1b,enb_read=%1b,addr_strob=%1b,mem_bus=%8h,mem_addr=%8h",$time,enb_write,enb_read,addr_strob,mem_bus,mem_addr);
end

endmodule
