`include "header.v"

module mem_controller(
input enb_write,
input enb_read,
input addr_strob,
input [31:0] mem_addr,
inout [31:0] mem_bus
);
parameter MEMSIZE = 512;
parameter WIDTH = 32;
reg[7:0]  MEM[0:MEMSIZE-1];

assign #40 mem_bus = (enb_read===1'b1 && addr_strob===1'b1) ? read_mem(mem_addr) : 32'bz;

initial begin
  $readmemh("v.out",MEM);       // Read v.out file into MEM.
//  dump_memory;
end

always @(posedge enb_write)
begin
    if(addr_strob)
      #40; write_mem(mem_addr,mem_bus);   
  #60;dump_memory;
end

function [WIDTH-1:0] read_mem;
  input [WIDTH-1:0] addr;               // the address from which to read

 // read_mem = {MEM[addr],MEM[addr+1],MEM[addr+2],MEM[addr+3]};
   read_mem = {MEM[addr+3],MEM[addr+2],MEM[addr+1],MEM[addr]};

endfunction     //


task write_mem; 
  input [WIDTH-1:0] addr;       // Address to which to write.
  input [WIDTH-1:0] data;       // The data to write.

  begin

  //  {MEM[addr],MEM[addr+1],MEM[addr+2],MEM[addr+3]} = data;
      {MEM[addr+3],MEM[addr+2],MEM[addr+1],MEM[addr]} = data;

  end 

endtask         // wri 

task dump_memory;
  integer k;
  begin
   for (k=0; k<MEMSIZE; k=k+4)
     begin
     $write("%h   %h %h %h %h\n", k,MEM[k],MEM[k+1],MEM[k+2],MEM[k+3]);
     end 
end
endtask

endmodule
