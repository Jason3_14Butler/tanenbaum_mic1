module decoder_assign (
input [3:0] decoder_input ,
output [15:0] decoder_output
);

//wire [15:0] decoder_output;
assign decoder_output = (1 << decoder_input);

endmodule
