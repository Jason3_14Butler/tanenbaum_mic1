/*
  © Copywrite Jason Butler 2017 GPL
*/


module decodertb;

reg [3:0] input1;

wire [15:0] out;

integer i;

decoder_assign uut (
.decoder_input(input1),
.decoder_output(out)
);

initial
begin
  $dumpfile("decodertb.vcd");
  $dumpvars(0,decodertb);
input1 = 0;
for(i=0;i<16;i=i+1) begin
  #20; input1=i;
end

#40;
end


initial
begin
$monitor("time = %2d, IN1=%2b,OUT=%4b",
$time,input1,out);
end

endmodule
