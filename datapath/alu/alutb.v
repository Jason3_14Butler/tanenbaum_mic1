/*
  © Copyright Jason Butler 2017 GPL
*/

`timescale 1ns / 1ps

module alutb;

reg [1:0] func;
reg ENA;
reg ENB;
reg INVA;
reg INC;
reg [31:0] A;
reg [31:0] B;

wire [31:0] alu_out;
wire carryout;

alu uut (
.func(func),
.ENA(ENA),
.ENB(ENB),
.INVA(INVA),
.INC(INC),
.A(A),
.B(B),
.alu_out(alu_out),
.carryout(carryout)
);

initial
begin
  $dumpfile("alutb.vcd");
  $dumpvars(0,alutb);

$display("testing alu");
func = 2'b01;ENA=1;ENB=0;INVA=0;INC=0;A=32'hdeadbeef;B=32'habababab;
#20;ENA=0;ENB=1;
#20;INC=1;func=2'b11;
#20;
end


initial
begin
$monitor("time = %2d,A=%8h B=%8h func=%2b ENA=%1b ENB=%1b INVA=%1b INC=%1b alu_out=%8h carryout=%1b",$time,A,B,func,ENA,ENB,INVA,INC,alu_out,carryout);
end

endmodule
