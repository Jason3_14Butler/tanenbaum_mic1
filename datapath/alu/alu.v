/*
  © Copyright Jason Butler 2017 GPL
*/

module alu(
  input [1:0] func,
  input ENA,
  input ENB,
  input INVA,
  input INC,
  input [31:0] A,
  input [31:0] B,
  output reg [31:0] alu_out,
  output reg carryout
);

/*
initial begin
func = 2'b0;
A = 32'h00000000;
end
*/

wire [5:0] alu_ctrl;

assign alu_ctrl[5] = func[1];
assign alu_ctrl[4] = func[0];
assign alu_ctrl[3] = ENA;
assign alu_ctrl[2] = ENB;
assign alu_ctrl[1] = INVA;
assign alu_ctrl[0] = INC;

always @ (func[0] or func[1] or ENA or ENB or INVA or INC or A or B)
begin
#10;
case (alu_ctrl)
6'b011000: alu_out=A;
6'b010100: alu_out=B;
6'b011010: alu_out=~A;
6'b101100: alu_out=~B;
6'b111100: {carryout, alu_out} = A + B;
6'b111101: {carryout, alu_out} = A + B + 1;
6'b111001: {carryout, alu_out} = A + 1;
6'b110101: {carryout, alu_out} = B + 1;
6'b111111: {carryout, alu_out} = B - A;
6'b110110: {carryout, alu_out} = B - 1;
6'b111011: {carryout, alu_out} = -A;
6'b001100: alu_out = A & B;
6'b011100: alu_out = A | B;
6'b010000: alu_out = 0;
6'b110001: alu_out = 1;
6'b110010: alu_out = -1;
endcase
end
endmodule 
