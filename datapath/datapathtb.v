/*
  © Copyright Jason Butler 2017 GPL
*/

`timescale 1ns / 1ps

module datapathtb;

`define CLOCK_PULSE 50
`define TESTX  36'b000000000111111111111111111111110000
`define TEST   36'b000000000000000000111111111111110000
//`define ILOAD3 36'b000000000000000101010000010000000100
`define ILOAD3 36'b000000000000001101010000010000000100
//`define LOADS  36'b000000000000000101000000010000000101
`define LOADS 36'h000140401
`define LOADPC 36'h000140201
`define DUPPC 36'h000350302
`define DUPPC_F 36'h000350312
`define DUP1 36'h000350505
`define TEST0  36'b000000000000000000000000000000000000
`define read   36'b000000000000000000000000000000100000
`define LOADM  36'b000000000000000101000000000100000100
`define write  36'h000000040

reg [35:0] MIR;
reg [31:0] mem_bus_data;
wire [31:0] mem_bus;
wire [31:0] mem_bus_addr;
wire [7:0] sequencer_bus;

assign mem_bus = mem_bus_data;
//oscilator osc(clk,clk_n);
datapath uut (
.MIR(MIR),
.mem_bus(mem_bus),
.mem_bus_addr(mem_bus_addr),
.sequencer_bus(sequencer_bus)
);

initial
begin
  $dumpfile("datapathtb.vcd");
  $dumpvars(0,datapathtb);

$display("testing datapath");
#5;mem_bus_data=32'hABABABAB;MIR=`read;
#`CLOCK_PULSE;MIR=`LOADS;
#`CLOCK_PULSE;MIR=`DUP1;mem_bus_data=32'bz;
#`CLOCK_PULSE;MIR=`write;
#`CLOCK_PULSE;mem_bus_data=32'hDEADBEEF;MIR=`read;
#`CLOCK_PULSE;MIR=`LOADPC;
#`CLOCK_PULSE;MIR=`DUPPC;mem_bus_data=32'bz;
#`CLOCK_PULSE;MIR=`write;
#`CLOCK_PULSE;mem_bus_data=32'h1A2B3C4D;MIR=`read;
#`CLOCK_PULSE;MIR=`LOADPC;
#`CLOCK_PULSE;MIR=`DUPPC_F;mem_bus_data=32'bz;
#`CLOCK_PULSE;MIR=`write;
#50;
end

initial
begin
$monitor("time = %2d, MIR=%9h,mem_bus=%8h,mem_bus_addr=%8h,sequencer_bus=%8h",$time,MIR,mem_bus,mem_bus_addr,sequencer_bus);
end

endmodule
