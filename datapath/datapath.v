/*
  © Copyright Jason Butler 2017 GPL
*/

`include "header.v"

module datapath
(
input [35:0] MIR,
inout [31:0] mem_bus,
output [31:0] mem_bus_addr,
output [7:0] sequencer_bus
);

`define SP_select_local MIR[26]

`define NEXT_ADDRESS MIR[35:27]
`define JAM MIR[26:24]
`define ALU MIR[23:16]
`define C_ctrl MIR[15:7]
`define MEM_ctrl MIR[6:4]
`define B_ctrl MIR[3:0]

/*
`define MAR_select MIR[7]
`define MDR_select MIR[8]
`define PC_select MIR[9]
`define SP_select MIR[10]
`define LV_select MIR[11]
`define CPP_select MIR[12]
`define TOS_select MIR[13]
`define OPC_select MIR[14]
`define H_select MIR[15]
*/

`define MEM_read MIR[5]
`define MEM_write MIR[6]
`define MEM_fetch MIR[4]

/*
`define INC MIR[16]
`define INVA MIR[17]
`define ENB MIR[18]
`define ENA MIR[19]
`define F1 MIR[20]
`define F0 MIR[21]
*/
`define alu_func MIR[21:20]

parameter B_BUS_PC_LINE = 2;
parameter B_BUS_MDR_LINE = 1;
parameter B_BUS_MBR_LINE = 3;
parameter B_BUS_MBRU_LINE = 4;
parameter B_BUS_SP_LINE = 5;
parameter B_BUS_LV_LINE = 6;
parameter B_BUS_CPP_LINE = 7;
parameter B_BUS_TOS_LINE = 8;
parameter B_BUS_OPC_LINE = 9;

wire [31:0] c_bus;
wire [31:0] b_bus;
wire [31:0] h_out;
wire [15:0] b_bus_ctrl_lines;
wire carryout;
wire N;
wire Z;

parameter DEL_alu = 5;//`Dx;
assign h_out = 32'hzzzzzzzz;
decoder_assign b_bus_decoder(`B_ctrl,b_bus_ctrl_lines);

// The registers from Figure 4-1, Structured Computer Organization
register_mar MAR(`MAR_select,`MEM_read,`MEM_write,c_bus,mem_bus_addr);
register_pc PC(`PC_select,b_bus_ctrl_lines[B_BUS_PC_LINE],`MEM_fetch,c_bus,mem_bus_addr,b_bus);
register_mbr MBR(b_bus_ctrl_lines[B_BUS_MBR_LINE],b_bus_ctrl_lines[B_BUS_MBRU_LINE],`MEM_fetch,mem_bus,b_bus,sequencer_bus); 
register_mdr MDR(`MDR_select,b_bus_ctrl_lines[B_BUS_MDR_LINE],`MEM_read,`MEM_write,0,c_bus,mem_bus,b_bus);
register SP(`SP_select,b_bus_ctrl_lines[B_BUS_SP_LINE],`SP_ID,c_bus,b_bus);
register LV(`LV_select,b_bus_ctrl_lines[B_BUS_LV_LINE],`LV_ID,c_bus,b_bus);

register CPP(`CPP_select,b_bus_ctrl_lines[B_BUS_CPP_LINE],0,c_bus,b_bus);
register TOS(`TOS_select,b_bus_ctrl_lines[B_BUS_TOS_LINE],0,c_bus,b_bus);
register OPC(`OPC_select,b_bus_ctrl_lines[B_BUS_OPC_LINE],0,c_bus,b_bus);
register_h H(`H_select,0,c_bus,h_out);

alu ALU(`alu_func,`ENA,`ENB,`INVA,`INC,h_out,b_bus,c_bus,carryout);

endmodule
