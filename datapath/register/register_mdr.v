module register_mdr (
input enable_c_bus, 
input enable_b_bus,
input mem_read,
input mem_write,
input [3:0] reg_id,
input [31:0] c_bus,
inout [31:0] mem_bus,
output reg [31:0] b_bus
); 

parameter DEL_y = `Dw;

reg [31:0] register_value;

assign mem_bus = mem_write ? register_value : 32'bz;

initial begin
    b_bus = 32'bz;
//    mem_bus = 32'bz;
    register_value = 0;
end

always @(posedge enable_c_bus or posedge enable_b_bus or posedge mem_read)
begin

b_bus <= enable_b_bus ? register_value : 32'bz;
//mem_bus <= mem_write ? register_value : 32'bz;

if (mem_read)
  #60;register_value <= mem_bus;

if (enable_c_bus) begin
   #DEL_y;register_value <= c_bus; 
end

//$display("b_bus: %1b c_bus: %1b mem_read: %1b mem_write: %1b mem register: %8h time: %2d",enable_b_bus,enable_c_bus,mem_read,mem_write,register_value,$time);
end


endmodule
