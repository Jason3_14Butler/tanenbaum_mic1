`include "header.v"

module register_h (
input enable_c_bus, 
input [3:0] register_id,
input [31:0] c_bus,
output reg [31:0] b_bus
); 

parameter DEL_y = `Dw;

initial begin
    b_bus = 0;
end

always @(posedge enable_c_bus)
begin
   #DEL_y;b_bus = c_bus;  
//  $display("%m register value: %h time: %2d",register_value,$time);
end

endmodule
