/*
  © Copywrite Jason Butler 2017 GPL
*/


module register_mdrtb;

reg [4:0] enable_c_bus;
reg mem_write;
reg mem_read;
reg [4:0] enable_b_bus;
reg [31:0] c_bus;
wire [31:0] b_bus;
wire [31:0] mem_bus;
reg [31:0] mem_bus_data;

assign mem_bus = mem_bus_data;

register uut1 (
.enable_c_bus(enable_c_bus[0]),
.enable_b_bus(enable_b_bus[0]),
.c_bus(c_bus),
.b_bus(b_bus)
);

register uut2 (
.enable_c_bus(enable_c_bus[1]),
.enable_b_bus(enable_b_bus[1]),
.c_bus(c_bus),
.b_bus(b_bus)
);

register uut3 (
.enable_c_bus(enable_c_bus[2]),
.enable_b_bus(enable_b_bus[2]),
.c_bus(c_bus),
.b_bus(b_bus)
);

register uut4 (
.enable_c_bus(enable_c_bus[3]),
.enable_b_bus(enable_b_bus[3]),
.c_bus(c_bus),
.b_bus(b_bus)
);

register_mdr uut5 (
.enable_c_bus(enable_c_bus[4]),
.enable_b_bus(enable_b_bus[4]),
.mem_read(mem_read),
.mem_write(mem_write),
.c_bus(c_bus),
.mem_bus(mem_bus),
.b_bus(b_bus)
);

initial
begin
  $dumpfile("register_mdrtb.vcd");
  $dumpvars(0,register_mdrtb);
enable_c_bus = 0;
enable_b_bus = 0;
mem_read=0;mem_write=0;
#20;c_bus = 32'hABABABAB;enable_c_bus[0] = 1;
#20;enable_c_bus[0] = 0;enable_b_bus[0] = 1;
#20;enable_b_bus[0]=0;mem_bus_data=32'hDEADBEEF;mem_read=1;
#20; mem_read=0;enable_b_bus[4]=1;
#40;
end


initial
begin
$monitor("time = %2d, enable_c_bus=%4b,enable_b_bus=%4b,c_bus=%8h,b_bus=%8h,mem_bus=%8h,mem_read=%1b,mem_write=%1b",
$time,enable_c_bus,enable_b_bus,c_bus,b_bus,mem_bus,mem_read,mem_write);
end

endmodulelu_func MIR[21:20]

