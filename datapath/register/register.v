`include "header.v"

module register (
input enable_c_bus, 
input enable_b_bus,
input [3:0] register_id,
input [31:0] c_bus,
output reg [31:0] b_bus
); 

parameter DEL_y = `Dw;

reg [31:0] register_value;

initial begin
    b_bus = 32'bz;
    case (register_id)
      `SP_ID : register_value = `STACK_BASE;
      `CPP_ID : register_value = `CONSTANT_BASE;
      `LV_ID : register_value = `LOCAL_BASE;
      default : register_value = 0;
    endcase 
end

always @(posedge enable_b_bus)
begin

b_bus <= enable_b_bus ? register_value : 32'bz;
end

always @(posedge enable_c_bus)
begin
if (enable_c_bus) begin
   #DEL_y; register_value <= c_bus;  
//  $display("%m register value: %h time: %2d",register_value,$time);
end
end

always @(negedge enable_b_bus)
begin
  b_bus <= 32'bz;
end

endmodule
