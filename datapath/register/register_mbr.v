`include "header.v"

module register_mbr (
input enable_b_bus,
input enable_b_bus_sign,
input mem_fetch,
input [31:0] mem_bus,
output reg [31:0] b_bus,
output [7:0] sequencer_bus
); 
parameter DEL_y = `Dw;
reg [7:0] register_value;
assign sequencer_bus = register_value;

initial begin
    b_bus = 32'bz;
    register_value = 0;
end

always @(posedge enable_b_bus_sign or posedge enable_b_bus or posedge mem_fetch)
begin

if (enable_b_bus)
  b_bus = register_value;
else if(enable_b_bus_sign)
  b_bus = sext(register_value);
else
  b_bus = 32'bz;

if (mem_fetch)
  #60; register_value <= mem_bus;

//$display("b_bus: %1b b_bus_sign: %1b mem_fetch: %1b, register: %8h time: %2d",enable_b_bus,enable_b_bus_sign,mem_fetch,register_value,$time);
end

always @(negedge enable_b_bus or negedge enable_b_bus_sign)
begin
  b_bus <= 32'bz;
end


function [31:0] sext;
  input [7:0] d_in;
  sext[31:0] = { {(24){d_in[7]}} ,d_in};
endfunction

endmodule
