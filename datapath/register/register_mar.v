module register_mar (
input enable_c_bus, 
input read,
input write,
input [31:0] c_bus,
output [31:0] mem_bus_addr
); 

parameter DEL_y = `Dw;

reg [31:0] register_value;

assign mem_bus_addr = (read ^ write) ? register_value : 32'bz;

initial begin
    register_value = 0;
end


always @(posedge enable_c_bus)
begin

//b_bus <= enable_b_bus ? register_value : 32'bz;

if (enable_c_bus) begin
   #DEL_y;register_value <= c_bus; 
end

//$display("b_bus: %1b c_bus: %1b fetch: %1b, mem_register: %8h, mem_bus_addr: %8h,b_bus: %8h, time: %2d",enable_b_bus,enable_c_bus,fetch,register_value,mem_bus_addr,b_bus,$time);
end

endmodule
