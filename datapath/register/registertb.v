/*
  © Copywrite Jason Butler 2017 GPL
*/


module registertb;

reg [3:0] enable_c_bus;
reg [3:0] enable_b_bus;
reg [31:0] c_bus;
wire [31:0] b_bus;

register uut1 (
.enable_c_bus(enable_c_bus[0]),
.enable_b_bus(enable_b_bus[0]),
.c_bus(c_bus),
.b_bus(b_bus)
);

register uut2 (
.enable_c_bus(enable_c_bus[1]),
.enable_b_bus(enable_b_bus[1]),
.c_bus(c_bus),
.b_bus(b_bus)
);

register uut3 (
.enable_c_bus(enable_c_bus[2]),
.enable_b_bus(enable_b_bus[2]),
.c_bus(c_bus),
.b_bus(b_bus)
);

register uut4 (
.enable_c_bus(enable_c_bus[3]),
.enable_b_bus(enable_b_bus[3]),
.c_bus(c_bus),
.b_bus(b_bus)
);


initial
begin
  $dumpfile("registertb.vcd");
  $dumpvars(0,registertb);
enable_c_bus = 0;
enable_b_bus = 0;
#20;c_bus = 32'hABABABAB;enable_c_bus[0] = 1;
#20;enable_c_bus[0] = 0;enable_b_bus[0] = 1;
#40;
end


initial
begin
$monitor("time = %2d, enable_c_bus=%4b,enable_b_bus=%4b,c_bus=%8h,b_bus=%8h ",
$time,enable_c_bus,enable_b_bus,c_bus,b_bus);
end

endmodule
