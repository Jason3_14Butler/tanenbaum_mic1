`include "header.v"

`define CLOCK_HIGH 40 
`define CLOCK_LOW 60
module mic1tb;

wire [35:0] MIR;
wire [31:0] mem_bus;
wire [31:0] mem_bus_addr;
wire [7:0] sequencer_bus;
reg clk;

`define MEM_WRITE MIR[6]
`define MEM_READ MIR[5]
`define MEM_FETCH MIR[4]

wire mem_read = `MEM_READ ^ `MEM_FETCH;
wire mem_strob = mem_read ^ `MEM_WRITE;

sequencer sequencer(.clk(clk),
                    .sequencer_bus(sequencer_bus),
                    .MIR(MIR)
                   );
mem_controller mem_controller(.enb_write(`MEM_WRITE),
               .enb_read(mem_read),
               .addr_strob(mem_strob),
               .mem_addr(mem_bus_addr),
               .mem_bus(mem_bus)
              );
datapath datapath(.MIR(MIR),
                  .mem_bus(mem_bus),
                  .mem_bus_addr(mem_bus_addr),
                  .sequencer_bus(sequencer_bus)
                 );
initial
begin
  $dumpfile("mic1tb.vcd");
  $dumpvars(0,mic1tb);

$display("Starting mic1....");
clk = 1;
#`CLOCK_HIGH;clk=~clk;
#`CLOCK_LOW;clk=~clk;
#`CLOCK_HIGH;clk=~clk;
#`CLOCK_LOW;clk=~clk;
#`CLOCK_HIGH;clk=~clk;
#`CLOCK_LOW;clk=~clk;
#`CLOCK_HIGH;clk=~clk;
#`CLOCK_LOW;clk=~clk;
#`CLOCK_HIGH;clk=~clk;
#`CLOCK_LOW;clk=~clk;
#`CLOCK_HIGH;clk=~clk;
#`CLOCK_LOW;clk=~clk;
#`CLOCK_HIGH;clk=~clk;
#`CLOCK_LOW;clk=~clk;
#`CLOCK_HIGH;clk=~clk;
#`CLOCK_LOW;clk=~clk;
end

initial
begin
$monitor("time =%2d,MIR=%9h,mem_bus=%8h,mem_bus_addr=%8h,sequencer_bus=%8h, mem_read=%1b, mem_strob=%1b", $time,MIR,mem_bus,mem_bus_addr,sequencer_bus,mem_read,mem_strob); 

end


endmodule
