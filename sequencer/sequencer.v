/*
  © Copyright Jason Butler 2017 GPL
*/

`include "header.v"

module sequencer(
input clk,
input [7:0] sequencer_bus,
output reg [35:0] MIR
);

`define C_SIG MIR[7]
`define JMPC MIR[26]
`define JAMN MIR[25]
`define JAMZ MIR[24]
`define NEXT_ADDRESS MIR[35:27] 
// B-bus MDR:0, PC:1, MBR:2, SP:3, LV:4
`define NOP1 36'h08000000
`define NOP1ADDR 9'h0
`define MAIN1 36'h04350212   
`define MAIN1ADDR 9'h1
`define ILOAD1 36'h0B0148006
`define ILOAD1ADDR 9'h15
`define ILOAD2 36'h0B83C00A4
`define ILOAD2ADDR 9'h16
`define ILOAD3 36'h0C0350485
`define ILOAD3ADDR 9'h17
`define ILOAD4 36'h0C8350242
`define ILOAD4ADDR 9'h18
`define ILOAD5 36'h008142011
`define ILOAD5ADDR 9'h19
`define BIPUSH1 36'h088050485
`define BIPUSH1ADDR 9'h10 
`define BIPUSH2 36'h090050212
`define BIPUSH2ADDR 9'h11
`define BIPUSH3ADDR 9'h12
`define BOOT1 36'h000000010
parameter ROMSIZE = (1<<8);
`define BOOT1ADDR (ROMSIZE -1)
//parameter DEL_y = `Dw;
//MIR = MIR_REG;
reg[35:0] ROM[ROMSIZE-1:0];
reg[8:0] MPC;

specify
  specparam DEL_y = `Dx;
  specparam DEL_z = `Dz;
  (clk => INC) = DEL_y;
  (clk => INVA) = DEL_y;
  (clk => ENB) = 30;
  (clk => ENA) = DEL_y;
  (clk => F1) = DEL_y;
  (clk => F0) = DEL_y; 
  (clk => MAR_select) = DEL_z;
  (clk => MDR_select) = DEL_z;
  (clk => PC_select) = DEL_z;
  (clk => SP_select) = DEL_z;
  (clk => LV_select) = DEL_z;
  (clk => CPP_select) = DEL_z;
  (clk => TOS_select) = DEL_z;
  (clk => OPC_select) = DEL_z;
  (clk => H_select) = DEL_z; 
endspecify

//reg[35:0] MIR_REG;

initial
begin
  MPC = `BOOT1ADDR;
  MIR = `BOOT1;
  ROM[`NOP1ADDR] = `NOP1;
  ROM[`MAIN1ADDR] = `MAIN1;
  ROM[`BIPUSH1ADDR] = `BIPUSH1;
  ROM[`BIPUSH2ADDR] = `BIPUSH2;
  ROM[`ILOAD1ADDR] = `ILOAD1;
  ROM[`ILOAD2ADDR] = `ILOAD2;
  ROM[`ILOAD3ADDR] = `ILOAD3;
  ROM[`ILOAD4ADDR] = `ILOAD4;
  ROM[`ILOAD5ADDR] = `ILOAD5;
  ROM[`BOOT1ADDR] = `BOOT1;
  //dump_rom;
end
//dump_rom;
always @(negedge clk)
begin
  MIR <= ROM[MPC];
end
always @(posedge clk)
begin
  if(`JMPC === 1)
    MPC <= sequencer_bus ^ `NEXT_ADDRESS;
  else
    MPC <= `NEXT_ADDRESS; 
  `C_SIG <= 0;
end

task dump_rom;
  integer k;
  begin
   for (k =0; k<ROMSIZE; k=k+1)
     begin
     $write("ROM[%d]; %h\n", k,ROM[k]);
     end
end
endtask
endmodule
