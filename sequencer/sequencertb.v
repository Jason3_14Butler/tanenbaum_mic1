/*
  © Copyright Jason Butler 2017 GPL
*/

`timescale 1ns / 1ps

module sequencertb;

`define TESTX  36'b000000000111111111111111111111110000
`define TEST   36'b000000000000000000111111111111110000
//`define ILOAD3 36'b000000000000000101010000010000000100
`define ILOAD3 36'b000000000000001101010000010000000100
`define LOADS  36'b000000000000000101000000010000000011
`define TEST0  36'b000000000000000000000000000000000000
`define read   36'b000000000000000000000000000000010000
`define LOADM  36'b000000000000000101000000000100000100

wire [35:0] MIR;
reg clk;
reg [7:0] sequencer_bus;

//assign mem_bus = mem_bus_data;
//oscilator osc(clk,clk_n);
sequencer uut (
.clk(clk),
.MIR(MIR),
.sequencer_bus(sequencer_bus)
);

initial
begin
  $dumpfile("sequencertb.vcd");
  $dumpvars(0,sequencertb);

$display("testing datapath");
clk=1;sequencer_bus=0;
#20;clk=~clk;
#20;clk=~clk;
#20;clk=~clk;
#20;clk=~clk;// sequencer_bus=9'h10;
#20;clk=~clk;
#20;clk=~clk;
#20;clk=~clk;sequencer_bus=9'h10;
#20;clk=~clk;
#20;clk=~clk;

end

initial
begin
$monitor("time = %2d, MIR=%9h,sequencer_bus=%2h,clk=%1b ",$time,MIR,sequencer_bus,clk);
end

endmodule
