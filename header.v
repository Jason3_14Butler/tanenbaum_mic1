`define CLOCK_PULSE 50
`define Dw `CLOCK_PULSE/4
`define Dx `CLOCK_PULSE/4 + `Dw
`define Dy `CLOCK_PULSE/4 + `Dx  //ALU and shifter
`define Dz `CLOCK_PULSE/4 + `Dy

`define SP_ID 4'h5
`define CPP_ID 4'h7
`define LV_ID 4'h8

`define STACK_BASE 32'h00000100
`define CONSTANT_BASE 32'h00000010
`define LOCAL_BASE 32'h00000014

`define INC MIR[16]
`define INVA MIR[17]
`define ENB MIR[18]
`define ENA MIR[19]
`define F1 MIR[20]
`define F0 MIR[21]

`define MAR_select MIR[7]
`define MDR_select MIR[8]
`define PC_select MIR[9]
`define SP_select MIR[10]
`define LV_select MIR[11]
`define CPP_select MIR[12]
`define TOS_select MIR[13]
`define OPC_select MIR[14]
`define H_select MIR[15]



