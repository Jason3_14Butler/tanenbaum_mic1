# README #


### What is this repository for? ###

This is a verilog implementation of the MIC1 processor described in chapter four of Tanenbaum's
Structured Computer Organization 5th edition.

### How do I get set up? ###

The processor can be built with Icarus Verilog version 0.9.7 and simulated with vvp. vcd files can be analysed with gtkwave.

To build run ***make*** from the top directory then ***make run***. It will automatically load the canned memory image. When the simulation finishes it will dump the memory and you can see that 0xdeadbeef was written to the stack. It uses a hard-coded stack base of 0x100.

You can analyze the simulation results ***gtkwave mic1tb.vcd***. Add the *MAR* register in gtkwave and you can see it step through the micro code.

Each subsystem has its own test bed. To test a subsystem cd into its directory and run make and make run there. 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact