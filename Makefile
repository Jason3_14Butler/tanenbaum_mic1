build:
	 iverilog -o mic1test mic1tb.v header.v datapath/datapath.v datapath/register/register.v datapath/register/register_mdr.v datapath/decoder/decoder.v datapath/register/register_h.v datapath/alu/alu.v datapath/register/register_mar.v datapath/register/register_pc.v datapath/register/register_mbr.v sequencer/sequencer.v mem_controller/mem_controller.v

run:
	vvp mic1test
